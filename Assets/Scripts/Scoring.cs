﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: Scoring.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/Scoring.cs
//Date: 26.05.2016
//Description: Script that keeps track of score

using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class Scoring : MonoBehaviour {

	[SerializeField] GameControler gameControler;
	public int HitValue = 100;
	public int ForceValue = 10000;
	//private float startingValue = 1.0f;
	private Color defaultColor;
	private Color triangleColor;
	private Color tempColor = Color.red;  // temporary color and which is applied on shader
	private Renderer RendComp;
	private bool runningCorourotine = false;

	[SerializeField] AudioClip Bump;
    private AudioSource audio;

    void Awake () {
    	audio = GetComponent<AudioSource>();
    }

	/// <summary>
	/// If ball colided with bumper add 1 point to score
	/// </summary>
	void OnCollisionEnter(Collision coll) {
		GameObject tempBall = coll.gameObject;
		if (tempBall.name == "Ball"){
			gameControler.scoreNum += gameControler.Multiplayer * HitValue;  // add 1 point
			gameControler.addMultiplier();  // add hit to array 
			ContactPoint contact = coll.contacts[0];  // gets where ball colided
			//Debug.Log("Normal: " + contact.normal.ToString());
			tempBall.GetComponent<Rigidbody>().AddForce(contact.normal * ForceValue);

			//Play sound
			audio.PlayOneShot(Bump);

			//check if bumper
			string pattern = "Bumber*";
			//string pattern2 = ".*Triangle";
			Match match = Regex.Match(contact.thisCollider.name, pattern);
			//Match match2 = Regex.Match(contact.thisCollider.name, pattern2);
			RendComp = GetComponent<Renderer>();
			//Debug.Log(contact.thisCollider.name + " " + match.Success.ToString());
			if (!runningCorourotine && match.Success) {
				defaultColor = RendComp.material.GetColor("_Color");  // saves previous color for later use
				tempColor.a = 0.8f;
				RendComp.material.SetColor("_Color", tempColor);  // sets color to red
				runningCorourotine = true;
				StartCoroutine(FadeColor(0.8f));  // starts couritine that fades bumper	
			}
			//else if (!runningCorourotine && match2.Success) {
			//	triangleColor = RendComp.material.GetColor("_Color");
			//	StartCoroutine(Blink(1.0f));
			//}
			
		}	
	}

	/// <summary>
	/// Fades bumber, by changing aplha value of color(standart shader rendering mode must be set to fade)
	/// </summary>
	IEnumerator FadeColor(float AlphaValue) {
		while (AlphaValue < 1.0f ) {
			AlphaValue += 0.3f * Time.deltaTime;  // increase value 
			tempColor.a = AlphaValue;
			RendComp.material.SetColor("_Color", tempColor);			
			yield return null;
		}
		RendComp.material.SetColor("_Color", defaultColor);  // sets back to default color
		runningCorourotine = false;
	}

	/*IEnumerator Blink(float AlphaValue) {
		while (AlphaValue > 0.7f ) {
			AlphaValue += 0.5f * Time.deltaTime;  // increase value 
			triangleColor.a = AlphaValue;
			RendComp.material.SetColor("_Color", triangleColor);			
			yield return null;
		}
		while (AlphaValue < 1.0f ) {
			AlphaValue += 0.5f * Time.deltaTime;  // increase value 
			triangleColor.a = AlphaValue;
			RendComp.material.SetColor("_Color", triangleColor);			
			yield return null;
		}
		runningCorourotine = false;
	}*/

}
