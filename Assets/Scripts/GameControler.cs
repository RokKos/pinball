﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: GameControler.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/GameControler.cs
//Date: 25.05.2016
//Description: Script that controles game

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameControler : MonoBehaviour {

	[SerializeField] GameObject endScreen;  // End screen to show user that is end of the game
	[SerializeField] GameObject hudScreen;  // hud
	[SerializeField] GameObject pauseScreen;  // pause
	[SerializeField] Text scoreText;
	[SerializeField] Image ImageStrength;
	[SerializeField] GameObject PotiskGameObj;
	[SerializeField] GameObject Ball;
	[SerializeField] GameObject Table;
	[SerializeField] GameObject powerUpGameObj;
	[SerializeField] GameObject CoinGameObj;
	[SerializeField] GameObject barieriGamObj;
	[SerializeField] GameObject inputFieldGameObj;
	[SerializeField] GameObject submitHolderGameObj;

	private int NumOfLifes = 3;
	private const int NumOfMaxLifes = 3;
	private const int MaxMult= 5;
	[SerializeField] GameObject[] heartsGameObj = new GameObject[NumOfMaxLifes];
	[SerializeField] GameObject[] multGameObj = new GameObject[MaxMult];

	private float backPosition = 10.0f;
	private float frontPosition = 5.0f;
	private float speedBack = 20.0f;
	private float speedFront = 80.0f;
	private bool inTouch = false;
	private int shootStrength = 0;
	private float shootDefaultPosition;
	private bool notReleased = false;
	public int scoreNum;  // Variable to keep track of score

	public int Multiplayer = 1;

	private float SpawnRatePowerUps = 15.0f;
	private float TimePassedPowerUp = 0.0f;
	private int maxNumPowerUp = 3;
	private int currNumPowerUp = 3;
	private Vector3 DefaultPosition = new Vector3(-4.432f,-0.2f,9.564f);
	private bool gameOn = true;

	private List<float> multiplierHits = new List<float>();  // one count multiplier by holding time of hits and poping them when they are over
	private List<int> tableOfHits = new List<int>() { 1,2,6,10,20};
	//struct for coins
	struct CoinHolder {
		public string name; // WARNING rename child of coin(cilinder) into CoinX
		public Vector3 position;
		public bool isActive;
		public float timeOfPickup;
	}

	private List<CoinHolder> CoinArr = new List<CoinHolder>(); 

	[SerializeField] AudioClip gameOver;
	[SerializeField] AudioClip coin;
    private AudioSource audio;

	void Awake() { 
		endScreen.SetActive(false);
		hudScreen.SetActive(true);
		pauseScreen.SetActive(false);
		submitHolderGameObj.SetActive(false);
		scoreNum = 0;
		NumOfLifes = NumOfMaxLifes;
		DefaultPosition = Ball.transform.position;
		gameOn = true;
		barieriGamObj.SetActive(false);
		shootDefaultPosition = PotiskGameObj.transform.position.x;
		Time.timeScale = 1.5f;
		GameObject[] AllCoins = GameObject.FindGameObjectsWithTag("Coin");
		for (int i = 0; i< AllCoins.Length; ++i) {
			GameObject selectedGameObject = AllCoins[i];
			//Debug.Log(selectedGameObject.name);
			CoinHolder tempCoin = new CoinHolder();
			tempCoin.name = selectedGameObject.name;
			tempCoin.position = selectedGameObject.transform.position;
			tempCoin.isActive = true;
			tempCoin.timeOfPickup = -1.0f;
			//Assining gameobject to arrray
			CoinArr.Add(tempCoin);
		}
		audio = GetComponent<AudioSource>();

	}

	/// <summary>
	/// Show player that is end of the game
	/// </summary>
	public void GameOver() {
		NumOfLifes--;
		if (NumOfLifes <= 0) {
			endScreen.SetActive(true);	
			hudScreen.SetActive(false);
			//if score is higher then the lowest one
			if(scoreNum > PlayerPrefsX.GetIntArray("HighScores", 1000, 10)[0]){
				submitHolderGameObj.SetActive(true);
				// Goes thru all child
				foreach (Transform child in submitHolderGameObj.transform) {
					// finds the right one
					if(child.name == "DisplayHighScore") {
						// and change his text
						child.GetComponent<Text>().text = "New High Score : " + scoreNum.ToString();
						break;
					}
					
				}
			}else{
				submitHolderGameObj.SetActive(false);
			}
		}else{
			audio.PlayOneShot(gameOver);
			Rigidbody rbBall = Ball.GetComponent<Rigidbody>();
			//Debug.Log(rbBall.velocity.ToString());
			//Debug.Log(rbBall.angularVelocity.ToString());
			rbBall.velocity = Vector3.zero;
	 		rbBall.angularVelocity = Vector3.zero;
			//Debug.Log(rbBall.velocity.ToString());
			//Debug.Log(rbBall.angularVelocity.ToString());
			Ball.transform.position = DefaultPosition;
			Multiplayer = 1;
			barieriGamObj.SetActive(false);
		}
		
		
	}

	/// <summary>
	/// Show score on update
	/// </summary>
	void Update() {

		ShowLifes();
		scoreText.GetComponent<Text>().text = "Score: " + scoreNum.ToString();
		if (Input.GetKey(KeyCode.Space)) {
			//Debug.Log(shootStrength.ToString());
			if (shootStrength < 100) {
				shootStrength++;	
			}
			
			notReleased = true;
		} else {
			if (notReleased && inTouch) {
				//Debug.Log(PotiskGameObj.transform.position.x.ToString());
				StartCoroutine(ShootBall(shootDefaultPosition, shootStrength));
				
			}
			if (shootStrength > 0) {
				shootStrength-= 4;
			}
			notReleased = false;
		}

		ImageStrength.fillAmount = (float)shootStrength/100.0f;

		if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) {
			StartCoroutine(MoveTable());
		}

		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (gameOn) {
				PausePlay();
			}
			else {
				ResumePlay();
			}
		}
		
		TimePassedPowerUp += Time.deltaTime;
		if(SpawnRatePowerUps < TimePassedPowerUp && currNumPowerUp < maxNumPowerUp) {
			TimePassedPowerUp = 0.0f;
			Instantiate(powerUpGameObj, new Vector3(10.0f, 0.6f, 20.0f), Quaternion.identity);
		}
		// Cheing for multiplayer and calculating it
		CheckMultiplierTime();
		int countMult = multiplierHits.Count;
		for (int i = 0; i < tableOfHits.Count; i++){
			if (countMult <= tableOfHits[i]) {
				//Debug.Log(tableOfHits[i].ToString());
				Multiplayer = i+1;
				break;
			}
		}
		ShowMuiltiplier();
		UpdateCoins();
		//Debug.Log("Multi: " + Multiplayer.ToString());
		//Debug.Log("Count: " + countMult.ToString());
		
	}

	/// <summary>
	/// Shoots ball
	/// </summary>
	IEnumerator ShootBall(float currPos, int distance) {
		float originalDestination = currPos;
		//Debug.Log("orig: " + originalDestination.ToString());
		float toWhereToGo = currPos + backPosition * (float)distance / 100.0f;
		//Debug.Log("where" + toWhereToGo.ToString());
		//first it goes back
		while (currPos < toWhereToGo) {
			currPos += Time.deltaTime * speedBack;
			PotiskGameObj.transform.position = new Vector3(currPos, PotiskGameObj.transform.position.y, PotiskGameObj.transform.position.z);
			//Debug.Log(PotiskGameObj.transform.position.x.ToString());
			yield return null;
		}
		//Debug.Log("--------------------");
		toWhereToGo = originalDestination - frontPosition * (float)distance / 100.0f;
		//Debug.Log(toWhereToGo.ToString());
		while (currPos > toWhereToGo) {
			currPos -= Time.deltaTime * speedFront;
			PotiskGameObj.transform.position = new Vector3(currPos, PotiskGameObj.transform.position.y, PotiskGameObj.transform.position.z);
			//Debug.Log(PotiskGameObj.transform.position.x.ToString());
			yield return null;
		}
		Ball.transform.TransformDirection(Vector3.forward * 10);
		float force = -7000 * (float)distance / 100.0f;
		Ball.GetComponent<Rigidbody>().AddForce(force, 0, 0);
		//inTouch = false;
		while (currPos < originalDestination) {
			currPos += Time.deltaTime * speedFront;
			PotiskGameObj.transform.position = new Vector3(currPos, PotiskGameObj.transform.position.y, PotiskGameObj.transform.position.z);
			//Debug.Log(PotiskGameObj.transform.position.x.ToString());
			yield return null;
		}
		//Debug.Log("Launch");
	}

	public void InTouch(bool state) {
		inTouch = state;
	}

	IEnumerator MoveTable() {
		int moveCount = 0;
		int sizeCount = 10;
		while (moveCount < sizeCount) {
			Table.transform.position += Vector3.forward * Time.deltaTime;
			moveCount++;
			yield return null;
		}
		moveCount = 0;
		while (moveCount < 2* sizeCount) {
			moveCount++;
			Table.transform.position += Vector3.back * Time.deltaTime;
			yield return null;
		}
		moveCount = 0;
		while (moveCount < sizeCount) {
			Table.transform.position += Vector3.forward * Time.deltaTime;
			moveCount++;
			yield return null;
		}		
	}

	void ShowLifes () {
		if(NumOfLifes > 0) {

			for (int i = 0; i < NumOfLifes; ++i) {
				heartsGameObj[i].SetActive(true);
			}
			for (int i = NumOfLifes; i < NumOfMaxLifes; ++i) {
				heartsGameObj[i].SetActive(false);
			}
		}
	}

	public void ResetLevel() {
		Application.LoadLevel(Application.loadedLevel);
	}

	public void GoToHighScores() {

		Application.LoadLevel("Menu");
	}

	public void ExitApllication () {
		Application.Quit();
	}

	public void ResumePlay () {
		pauseScreen.SetActive(false);
		hudScreen.SetActive(true);	
		Time.timeScale = 1.5f; //change to current time scale
		gameOn = !gameOn;
	}

	public void PausePlay () {
		pauseScreen.SetActive(true);
		hudScreen.SetActive(false);
		Time.timeScale = 0;
		gameOn = !gameOn;
	}

	private void CheckMultiplierTime() { 
		//printValues();
		for (int i = 0;  i < multiplierHits.Count; ++i){
			if( multiplierHits[i] <= 0.0f) {
				multiplierHits.Remove(multiplierHits[i]);  // remove this multiplier 
			}else{
				multiplierHits[i] -= Time.deltaTime;
			}
			
		}
		//printValues();
	}

	public void  printValues() {
		foreach ( float obj in multiplierHits ) {
        	Debug.Log( " " + obj.ToString());
      	}
   }

   	public void addMultiplier() {
   		switch (Multiplayer) {
   			case 1:
   			  multiplierHits.Add(10);
   			  break;
   			case 2:
   			  multiplierHits.Add(5);
   			  break;
   			case 3:
   			  multiplierHits.Add(3);
   			  break;
   			case 4:
   			  multiplierHits.Add(2);
   			  break;
   			case 5:
   			  multiplierHits.Add(1);
   			  break;
   		}   		

   	}
	private void ShowMuiltiplier() {
		for (int i = 0; i < Multiplayer; ++i){
			multGameObj[i].SetActive(true);
		}
		for (int i = Multiplayer; i < MaxMult; ++i){
			multGameObj[i].SetActive(false);
		}
   	}
   	/// <summary>
   	/// When coin is pick up
   	/// WARNING rename child of coin(cilinder) into CoinX
   	/// </summary>
   	public void CoinPickUp(string name) {
   		for (int i = 0; i < CoinArr.Count; ++i) {
   			if (CoinArr[i].name == name){
   				CoinHolder tempCoin = new CoinHolder();
   				tempCoin.name = CoinArr[i].name;
   				tempCoin.position = CoinArr[i].position;
   				tempCoin.isActive = false;
   				tempCoin.timeOfPickup = 10.0f;
   				CoinArr[i] = tempCoin;
   				break;
   			}
   		}
   		
   	}

   	public void UpdateCoins() {
   		for (int i = 0; i < CoinArr.Count; ++i) {
   			if (!CoinArr[i].isActive && CoinArr[i].timeOfPickup <= 0.0f) {
   				CoinHolder tempCoin = new CoinHolder();
   				tempCoin.name = CoinArr[i].name;
   				tempCoin.position = CoinArr[i].position;
   				tempCoin.isActive = true;
   				tempCoin.timeOfPickup = -1;
   				CoinArr[i] = tempCoin;
   				GameObject newCoin = Instantiate(CoinGameObj, CoinArr[i].position, Quaternion.identity) as GameObject;
   				newCoin.transform.Rotate(new Vector3(90.0f, 90.0f, 0), Space.World);

   				foreach (Transform tran in newCoin.transform) {
   					if (tran.name == "Coin") {
   						tran.name = CoinArr[i].name;
   						break;
   					}
   					
   				}
   			}else if (!CoinArr[i].isActive && CoinArr[i].timeOfPickup > 0.0f){
   				CoinHolder tempCoin = new CoinHolder();
   				tempCoin.name = CoinArr[i].name;
   				tempCoin.position = CoinArr[i].position;
   				tempCoin.isActive = CoinArr[i].isActive;
   				tempCoin.timeOfPickup = CoinArr[i].timeOfPickup - Time.deltaTime;
   				CoinArr[i] = tempCoin;

   			}

   		}
   	}
   	public void CloseBarier() {
   		StartCoroutine(Wait());
   	}

   	IEnumerator Wait() {
		yield return new WaitForSeconds(3);
		barieriGamObj.SetActive(true);
	}

	public void SubmitHighScore() {
		int sendNum = scoreNum;
		string sendName = "";
		foreach (Transform child in inputFieldGameObj.transform) {
			if(child.name == "Text") {
				sendName = child.GetComponent<Text>().text;
				break;
			}
		}
		//PlayerPrefs.SetInt("Score", scoreNum);
		//PlayerPrefs.SetString("NameOfPlayer", sendName);
		// Goes thru all child
		foreach (Transform child in submitHolderGameObj.transform) {
			// finds the right one
			if(child.name == "DisplayHighScore") {
				// and change his text
				child.GetComponent<Text>().text = "High Score Submited";
				break;
			}
			
		}
		CompareScore(sendNum, sendName);
	}


	/// <summary>
	/// Function that compares score to highscores
	/// </summary>
	public void CompareScore(int score, string name) {
		//int score = PlayerPrefs.GetIntArray("Score", -1);
		//string name = PlayerPrefs.GetIntArray("Name", "");

		//if it is empty then set it to 10 values of 1000
		int[] tempArr = PlayerPrefsX.GetIntArray("HighScores", 1000, 10);
		string[] tempStr = PlayerPrefsX.GetStringArray("HighNames", "", 10);
		int posToPlace = -1;
		for (int i = 0; i < tempArr.Length; ++i) {
			if ( tempArr[i] >= score) {
				posToPlace = i-1;
				break;
			}
		}
		//Debug.Log("pos: "+ posToPlace.ToString());
		if (posToPlace != -1){
			int[] endArr = new int[tempArr.Length];
			string[] endStr = new string[tempStr.Length];
			for (int i = 1; i <= posToPlace; ++i) {
				endArr[i-1] = tempArr[i];
				endStr[i-1] = tempStr[i]; // check
				//Debug.Log(tempArr[i].ToString());
				//Debug.Log(tempStr[i]);
			}
			endArr[posToPlace] = score;
			endStr[posToPlace] = name;
			//Debug.Log("-------------------");
			for (int i = posToPlace+1; i < tempArr.Length; ++i) {
				endArr[i] = tempArr[i];
				endStr[i] = tempStr[i]; // check
				//Debug.Log(tempArr[i].ToString());
				//Debug.Log(tempStr[i]);
			}
			PlayerPrefsX.SetIntArray("HighScores", endArr);
			PlayerPrefsX.SetStringArray("HighNames", endStr);	
		}	
	}

	public void PlayCoin(){
		audio.PlayOneShot(coin);
	}

	public void PowerUpMult() {
		for (int i = 0; i < 21; ++i) {
			multiplierHits.Add(10.0f);
		}
	}
}