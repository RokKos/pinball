﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: DetectEnd.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/DetectEnd.cs
//Date: 25.05.2016
//Description: Script that detects when ball goes out of bounds

using UnityEngine;
using System.Collections;

public class DetectEnd : MonoBehaviour {

	[SerializeField] GameControler gameControler;

	void OnCollisionEnter(Collision coll) {
		if (coll.gameObject.name == "Ball"){
			gameControler.GameOver();
		}	
	}
}
