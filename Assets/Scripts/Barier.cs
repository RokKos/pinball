﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: Barier.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/Barier.cs
//Date: 06.06.2016
//Description: Script that controls barier closing and opening
using UnityEngine;
using System.Collections;

public class Barier : MonoBehaviour {
	
	[SerializeField] GameControler gameControler;
	void OnTriggerEnter(Collider coll) {
		if (coll.gameObject.name == "Ball") {
			gameControler.CloseBarier();
		}
	}

}
