﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: TouchBall.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/TouchBall.cs
//Date: 31.05.2016
//Description: Script that ches if 
using UnityEngine;
using System.Collections;

public class TouchBall : MonoBehaviour {

	[SerializeField] GameControler gameControler;

	/// <summary>
	/// If ball is in touch
	/// </summary>
	void OnCollisionEnter(Collision coll) {
		if (coll.gameObject.name == "Ball") {
			Debug.Log("Touching");
			gameControler.InTouch(true);
		}	
	}

	void OnCollisionExit(Collision coll) {
        if(coll.gameObject.name == "Ball") {
        	gameControler.InTouch(false);
        	Debug.Log("Not Touching");
        }
    }
}
