﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: UIController.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/UIController.cs
//Date: 05.06.2016
//Description: Script that controlles UI actions
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class UIController : MonoBehaviour {

	private Dictionary<string, GameObject> AllMenus = new Dictionary<string, GameObject> (); //dictionary of all menus
	private string FirstMenu = "homeMenu"; //string represents first menu that will show infront of user
	private string regexSearchPatternPanel = "Panel"; //string by which are searched menus
	private List<string> previousMenu = new List<string>(){"homeMenu"}; //string that represents previous menu
	private int[] HighScoreDefaultValues = {1000, 3000, 5000, 10000, 50000, 100000, 500000, 1000000, 10000000, 1000000000};
	private string[] HighScoreDefaultNames = {"Flavijan", "Ana", "Lucija", "Jože", "Žan", "Lučka", "Robert", "Jasmina", "Julija", "Rok"};

	[SerializeField] GameObject[] HighScoresText = new GameObject[10];
	[SerializeField] GameObject creditsText;
	void Awake () {
		//Geting all panels by searching with regex expresion
		Time.timeScale = 1.5f;
		GameObject[] AllGameObjects = GameObject.FindGameObjectsWithTag(regexSearchPatternPanel);
		//HighScoresText = GameObject.FindGameObjectsWithTag("HighScore");
		for (int i = 0; i< AllGameObjects.Length; ++i) {
			GameObject selectedGameObject = AllGameObjects[i];
			//Debug.Log(selectedGameObject.name);
			//Assining gameobject to name in dictionary
			AllMenus[selectedGameObject.name] = selectedGameObject;
		}
		//enable hometMenu
		enableOneMenu (FirstMenu);

		// Seting high scores
		// if array is not set then set it
		//PlayerPrefs.DeleteAll();
		if (PlayerPrefsX.GetIntArray ("HighScores", -1, 1)[0] == -1) {
			Debug.Log("Seting highscores");
			PlayerPrefsX.SetIntArray ("HighScores", HighScoreDefaultValues);
			PlayerPrefsX.SetStringArray ("HighNames", HighScoreDefaultNames);
		}
		//UpdateHighScores();
	}

	/// <summary>
	/// Checks if back button is clicked and then goes to previous Menu
	/// </summary>	
	void Update(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			BackMenu();
		}
	}

	public void ExitApllication () {
		Application.Quit();
	}

	/// <summary>
	/// Goes to the main level when clicked on button.
	public void gotoMainLevel(){
		StartCoroutine(LoadNewScene("Main"));
	}


	/// <summary>
	/// Loads new scene in background while showing loading screen
	/// </summary>
	IEnumerator LoadNewScene(string nameOfScene) {
		// Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = Application.LoadLevelAsync(nameOfScene);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone) {
        	//rotate image to represent that content is loading
        	//loadingPercentageText.text = (async.progress * 100.0f).ToString() + "%";
        	//LoadingBarProgress(0.01f);
        	//loadingSceneImage.transform.RotateAround(loadingSceneImage.transform.position,  Vector3.forward, 20);
        	//Debug.Log(async.progress);
            yield return null;
        }
    }


    /// <summary>
	/// Function that enables one menu and disables other ones.
	/// Ko urejas to kodo pazi na imenovanje panelov oz. menujov in gumbov, ki kazejo na ta menu.
	/// Morata biti istega imena razlikujeta se lahko samo na koncu z "Button" in "Menu"
	/// Be very careful when naming panels(menus) and buttons which show on menu.
	/// They must have same name and only differ in appendix with "Button" and "Menu"
	/// </summary>
	/// <param name="nameOfMenu">Name of menu.</param>
	void enableOneMenu(string nameOfMenu){
		//Going thru every item in dictionary
		foreach(KeyValuePair<string, GameObject> menu in AllMenus){
			//checks name
			if(menu.Key == nameOfMenu){
				//and set active
				menu.Value.SetActive(true);
				if(menu.Key == "HighScoresMenu") {
					UpdateHighScores();
				}else if(menu.Key == "CreditsMenu") {
					StartCoroutine(moveTextUp());
				}

			}else{
				//or unactive
				menu.Value.SetActive(false);
			}
		}
	}
	
	//Function that changes selected menu to navigation menu
	public void gotoMenu(Button button){
		string regexButton = "_Button";
		//Remove substing from string in this case "_Button"
		string menuName = button.name.Remove(button.name.IndexOf(regexButton), regexButton.Length);
		//dont go to selection
		//beacuse you dont want to pick twice
		previousMenu.Add(menuName + "Menu"); //add to previousMenu array so we know from where we came
		//printPreviousMenu();
		enableOneMenu(menuName + "Menu");
	}

	/// <summary>
	/// Update Values on High Scores
	/// </summary>
	private void UpdateHighScores() {
		int[] highArr = PlayerPrefsX.GetIntArray("HighScores", 1000, 10);
		string[] nameArr = PlayerPrefsX.GetStringArray("HighNames", "", 10);
		for (int i = 0; i < HighScoresText.Length; ++i) {
			int index = HighScoresText.Length - i - 1;
			HighScoresText[i].GetComponent<Text>().text = String.Format("{0}.		{1}{2}{3}", i+1, nameArr[index], new String(' ', 4 + (6-nameArr[index].Length)), highArr[index]);
		}
	}

	/// <summary>
	/// Funktion that moves text up in the screen
	/// </summary>
	IEnumerator moveTextUp(){
		float ScreenHeight = Screen.height;//get scren height
		float moveRate = 1.5f;//tells how fast text is moving
		//goes to half of the screnn size
		while(creditsText.transform.position.y < ScreenHeight * 3.6f){
			//move text up by move rate
			//Debug.Log(creditsText.transform.position);
			creditsText.transform.position = new Vector3(creditsText.transform.position.x, creditsText.transform.position.y + moveRate, creditsText.transform.position.z);
			//this line keep proces runing
			yield return null;
		}
		//ends proces
		yield return new WaitForSeconds(0.01f);

	}

	public void BackMenu(){
		int lastElement = previousMenu.Count - 1;
		//Debug.Log(lastElement.ToString());\
		//Debug.Log(previousMenu[lastElement-1]);
		if(lastElement > 0){
			enableOneMenu(previousMenu[lastElement-1]); //get last menu
			previousMenu.RemoveAt(lastElement); //remove it so we dont go to that menu again

		}
		else{
			//if its in main menu and there is no previous menus and escape(back in mobile) is pressed
			//then exit the aplication
			ExitApllication();
		}

	}
}	
