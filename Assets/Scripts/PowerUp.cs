﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: PowerUp.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/PowerUp.cs
//Date: 04.06.2016
//Description: Script that controls powerups

using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public int TypeOfPowerUp; // 1 slow down, 2 speed up

	[SerializeField] GameControler gameControler;

	void Update() {
		transform.Rotate(new Vector3(0,1,0) * 100 * Time.deltaTime, Space.World);
	}

	/// <summary>
	/// If ball colided with powerUp 
	/// </summary>
	void OnCollisionEnter(Collision coll) {
		if (coll.gameObject.name == "Ball"){
			if (TypeOfPowerUp == 1) {
				StartCoroutine(SlowDownTime());	
			}else if (TypeOfPowerUp == 2) {
				StartCoroutine(SpeedUpTime());
			}
			
			gameControler.PowerUpMult();
			Destroy(gameObject);
		}	
	}
 
	IEnumerator SpeedUpTime() {
		Time.timeScale = 3.0f;
		yield return new  WaitForSeconds(5);
		while (Time.timeScale > 1.5f) {
			Time.timeScale -= 0.1f;
			yield return null;
		}
		Time.timeScale = 1.5f;
	}

	IEnumerator SlowDownTime() {
		Time.timeScale = 0.5f;
		yield return new  WaitForSeconds(1);
		while (Time.timeScale < 1.0f) {
			Time.timeScale += 0.1f;
			yield return null;
		}
		Time.timeScale = 1.0f;
	}

}
