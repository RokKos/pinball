﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: Flipper.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/Flipper.cs
//Date: 24.05.2016
//Description: Script for managing flipper 

using UnityEngine;
using System.Collections;

public class Flipper : MonoBehaviour {
	
	[SerializeField] HingeJoint hingeJoint;  // serialize hinge joint
	public KeyCode nameOfKey = KeyCode.LeftArrow;
	public float defaultPosition  = 25.0f;  // default postion
	public float pressedPosition = -25.0f;  // pressed postion
	private float springStrenght = 20000.0f;  // how fast is flipper moving
	private float springDamper = 100.0f;
	[SerializeField] AudioClip Paddle;
    private AudioSource audio;
	/// <summary>
	/// On awake set hinge joint to use spring
	/// </summary>
	void Awake() {
		hingeJoint.useSpring = true;
		audio = GetComponent<AudioSource>();
	}

	/// <summary>
	/// On every update checks if key was pressed
	/// if so then move flipper to preesed postion
	/// else move it to default postion
	/// </summary>
	void Update () {

		JointSpring springMove = new JointSpring();
	
		springMove.spring = springStrenght;
		springMove.damper = springDamper;

		if (Input.GetKeyDown(nameOfKey)) {
			//Play sound
			audio.PlayOneShot(Paddle);
		}

		if (Input.GetKey(nameOfKey)) {
			springMove.targetPosition = pressedPosition;
		}
		else {
			springMove.targetPosition = defaultPosition;
		}

		hingeJoint.spring = springMove;
	}
}
