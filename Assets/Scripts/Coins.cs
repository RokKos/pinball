﻿//Author: Rok Kos <rocki5555@gmail.com>
//File: Coins.cs
//File path: /D/Documents/Unity/Pinball/Assets/Scripts/Coins.cs
//Date: 05.06.2016
//Description: Script that controls coins
using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour {

	GameControler gameControler;
	public int HitValue = 200;
	private float yMove = 0.1f;
	private int Direction = 1;
	private float currentMove = 0.0f;

	void Awake(){
		gameControler = GameObject.FindGameObjectWithTag("GameControler").GetComponent<GameControler>();
	}	

	void OnTriggerEnter(Collider coll) {
		GameObject tempBall = coll.gameObject;
		if (tempBall.name == "Ball"){
			//play sound
			gameControler.PlayCoin();
			gameControler.scoreNum += gameControler.Multiplayer * HitValue;  // add 1 point
			gameControler.addMultiplier();  // add hit to array
			gameControler.CoinPickUp(gameObject.name);
			//delete whole object so that it does not come to memory leak
			Destroy(transform.parent.gameObject);
		}
	}

	void Update(){
		if(Time.timeScale == 1.5f) {
			transform.Rotate(new Vector3(0,1,0) * 100 * Time.deltaTime, Space.World);
			if (currentMove > yMove) {
				Direction *= -1;
				currentMove = 0;
			}
			currentMove += Time.deltaTime * 0.1f;
			transform.position = new Vector3(transform.position.x, transform.position.y + currentMove * Direction, transform.position.z);
		}
	}	
}
